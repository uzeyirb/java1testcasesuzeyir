package Java1TestCases;

public class uzeyirTC003 {

	public static void main(String[] args) {
		// Given int a = 55 and int b = 66
		// When we swap int values
		// Then int a will be 66 and int b will be 55
		int a = 55;
		int b = 66;
		System.out.println("a was " + a);
		System.out.println("b was " + b);

		int temp = a;
		a = b;
		b = temp;

		System.out.println("b is now " + b);
		System.out.println("a is now " + a);
		// a and b are copies of the original values.
		// The changes we made here won't be visible to the caller.

	}

}
