package Java1TestCases;

public class uzeyirTC002 {

	public static void main(String[] args) {
		// Given Euro-Dollar conversion rate is $1=0.9 Euro
		// When the dollar in the amound of 69 in string,
		// Then the euro conversion should be 62.23

		String dollar = "69";
		int idollar = Integer.parseInt(dollar);
		
		double rate = 0.9;
		double convertedEuros = idollar * rate;
		
		System.out.println("Converted amount is " + convertedEuros);
		if (convertedEuros == 62.23) {
			System.out.println("Conversion rate is correct");
			
		}
		else {
			System.out.println("Conversion in not correct");
		}
	
	}

}
